# GroEL-proteotyping
This repository contains the Python script for protein/taxonomy inference used in GroEL-proteotyping. For further information please read: Publication pending
This script is designed to be used with the collapsed output from a customized MetaProSIP workflow ([Jehmlich & von Bergen, 2020](https://doi.org/10.1007/978-3-319-50391-2_17)) on the Galaxy platform ([Afgan et al., 2018](https://doi.org/10.1093/nar/gky379)) for peptide identification.
On default, GroEL protein groups are filtered by a TopRank_Count > 4 and taxonomy is evaluated at the family level.

The script can be adapted for other proteins, search engines, or filtering mechanisms upon request.
## Method

<img src="https://www.mdpi.com/ijms/ijms-24-15692/article_deploy/html/images/ijms-24-15692-g005.png" width="800" height="auto" />

Graphical abstract of GroEL-proteotyping ([Klaes et al., 2023](https://www.mdpi.com/1422-0067/24/21/15692/html))

v2.0.0 can also be used for GroEL-proteotyping-based stable isotope probing (GroEL-SIP). For this purpose, the count of detected labeled GroEL peptides, as well as their median relative isotope abundance (RIA), and median labelling ratio (LR) are calculated. The scripts differentiates between unique detected peptides and peptides shared between different taxonomic groups.

## Getting started
### Version requirements:
 - Python >= 3.8.8 
 - pandas >= 1.2.4
 - numpy >= 1.23.5
 
 The script has only been tested on Windows.

### Necessary input datafiles:
|Description|Filename|Source|
|--|--|--|
| target database | GroEL_database.tsv | [PXD046460](https://www.ebi.ac.uk/pride/archive/projects/PXD046460) |
| contaminant database| cRAP_database.tsv | [PXD046460](https://www.ebi.ac.uk/pride/archive/projects/PXD046460) |
| identified peptides | MetaProSIP_Output.tsv | generate from your mzML files with the Galaxy Workflow or download test files from [PXD046460](https://www.ebi.ac.uk/pride/archive/projects/PXD046460) |


### Usage:
Download and execute the python script using your python distribution (e.g. [Anaconda](https://www.anaconda.com/)).

For Anaconda: 
1. Open the Anaconda prompt
2. (Install pandas by typing: `pip install pandas`)
3. Navigate to the directory of the python script by using cd (e.g. `cd C:\Users\admin\Downloads\`)
4. Execute the python script by typing: `python GroEL-proteotyping.py`

You will then be asked to select the input datafiles and the output directory. The script will then create five files for each sample from which the last has been used for population analysis:

1. Intermediate file with peptide hits assigned to acession numbers of the target database (sampleName_intermediate.tsv)
2. File with peptide hits assigned to acession numbers of the contaminant database (sampleName_contaminants.tsv)
3. Unfiltered output of GroEL protein groups (sampleName.tsv)
4. Quantification intermediate (sampleName_quant_intermediate.tsv)
5. Filtered output (default: TopRankCount > 4) of taxonomic groups (default: familiy level)(sampleName_family_based_min5.tsv)
 
## How to cite
Klaes, S.; Madan, S.; Deobald, D.; Cooper, M.; Adrian, L. GroEL-Proteotyping of Bacterial Communities Using Tandem Mass Spectrometry. Int. J. Mol. Sci. 2023, 24, 15692. https://doi.org/10.3390/ijms242115692 

Klaes, S.; Madan, S.; Deobald, D.; Cooper, M.; Adrian, L. Revealing taxonomy, activity, and substrate assimilation in mixed bacterial communities by GroEL-proteotyping-based stable isotope probing. iScience. 2024, 27, 111249. https://doi.org/10.1016/j.isci.2024.111249 

## Contact information
- Simon Klaes (https://orcid.org/0009-0000-8653-0675)
- Lorenz Adrian (https://orcid.org/0000-0001-8205-0842)
