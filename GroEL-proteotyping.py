#!/usr/bin/env python
# coding: utf-8


# This script was developed by Simon Klaes and Lorenz Adrian for GroEL-SIP.

import itertools
from itertools import product
import pandas as pd
from pandas.core.common import SettingWithCopyWarning
import warnings
import tkinter as tk
from tkinter import filedialog
import os
import statistics
import numpy as np

# This function generated the median of values ignoring 0.
def median_ignore_empty_and_zero(values):
    non_empty_values = [v for v in values if not pd.isnull(v) and v != 0]
    return np.median(non_empty_values) if non_empty_values else np.nan


# This  function generates all possible peptide sequences due to the indistinguishability of leucine and isoleucine.
def make_patterns(s,):
    IndistinguishableAminoAcids = "IL"
    seq = list(s)
    indices = [i for i, c in enumerate(seq) if c in IndistinguishableAminoAcids]
    for t in product(IndistinguishableAminoAcids, repeat=len(indices)):
        for i, c in zip(indices, t):
            seq[i] = c
        yield "".join(seq)


# This function takes a list as an input and returns the most frequent value in the list.
def most_frequent(List,):
    return max(set(List), key=List.count)


# This function takes a list as an input and returns the proportion of the most frequent value in the list.
def most_frequent_frequency_proportion(list_data,):
    stat_frequency = {}
    stat_proportion = {}
    total = len(list_data)
    for e in list_data:
        if str(e) in stat_frequency:
            stat_frequency[str(e)] += 1
        else:
            stat_frequency[str(e)] = 1
    for key, value in stat_frequency.items():
        stat_proportion[key] = value / total
    return max(stat_proportion.values())


def remove_duplicates_from_list(x):  # This function removes duplicates from a list
    return list(dict.fromkeys(x))


def get_duplicates_from_list(L):  # This function returns duplicate values from a list
    seen = set()
    seen2 = set()
    seen_add = seen.add
    seen2_add = seen2.add
    for item in L:
        if item in seen:
            seen2_add(item)
        else:
            seen_add(item)
    return list(seen2)


def get_intersection_from_two_lists(
    x, y
):  # This function returns the intersecition of two lists
    return list(set(x).intersection(y))


warnings.filterwarnings("ignore", category=SettingWithCopyWarning)

# ask for all paths needed for the script
root = tk.Tk()
root.withdraw()
collapsed_MetaProSIP_Output_filename = filedialog.askopenfilename(
    filetypes=[("collapsed MetaproSIP Output", ".tsv .csv")],
    title="Select the collapsed MetaProSIP Output",
)
target_database_filename = filedialog.askopenfilename(
    title="Select the target tsv-database"
)
cRAP_database_filename = filedialog.askopenfilename(
    title="Select the contaminants tsv-database"
)
directory_for_the_output_of_this_script = filedialog.askdirectory(
    title="Select the folder to save the results in"
)

# load collapsed_MetaProSIP_Output
print(
    "Loading collapsed MetaProSIP Output from " + collapsed_MetaProSIP_Output_filename
)
collapsed_MetaProSIP_Output = pd.read_csv(
    collapsed_MetaProSIP_Output_filename, sep="\t"
)
print("Collapsed MetaProSIP Output loaded successfully")

# load the trypsin-digest tsv of target database and contaminant database in the respective dataframe
print("Loading target database from " + target_database_filename)
df_target_database = pd.read_csv(target_database_filename, sep="\t")
print("Target database loaded successfully")
print("Loading cRAP database from " + cRAP_database_filename)
df_contaminants = pd.read_csv(cRAP_database_filename, sep="\t")
print("cRAP database loaded successfully")

# create a set (non-redundant) of all tryptics peptides in the target database and contaminant database, respectively
all_tryp_peps_set = set(
    itertools.chain.from_iterable(
        df_target_database.TrypPeps.apply(lambda x: x[2:-2].split("', '"))
    )
)
all_contaminant_peps_set = set(
    itertools.chain.from_iterable(
        df_contaminants.TrypPeps.apply(lambda x: x[2:-2].split("', '"))
    )
)

# convert the tryptic peptides column to lists to facilitate counting of hits
df_target_database["TrypPeps"] = df_target_database.TrypPeps.apply(
    lambda x: x[2:-2].split("', '")
)
df_contaminants["TrypPeps"] = df_contaminants.TrypPeps.apply(
    lambda x: x[2:-2].split("', '")
)

# MetaProSIP Output files are loaded subsequently and peptide sequences are extracted to the lists "found_seqs" and "found_seqs_labeled".
# Modifications (Carbamidomythylation and Oxidation) are being removed
sample_names = collapsed_MetaProSIP_Output["Sample"].unique().tolist()
for sample_name in sample_names:
    df_database = df_target_database
    found_seqs = []
    print(
        "Loading sample ",
        sample_names.index(sample_name) + 1,
        "/",
        len(sample_names),
        sample_name,
    )
    MetaProSIP_Output = collapsed_MetaProSIP_Output[
        collapsed_MetaProSIP_Output["Sample"] == sample_name
    ]
    python_output_filename = os.path.join(
        directory_for_the_output_of_this_script,
        sample_name.replace(".mzML", "") + ".tsv",
    )
    python_intermediate_filename = os.path.join(
        directory_for_the_output_of_this_script,
        sample_name.replace(".mzML", "") + "_intermediate.tsv",
    )
    python_contaminants_output_filename = os.path.join(
        directory_for_the_output_of_this_script,
        sample_name.replace(".mzML", "") + "_contaminants.tsv",
    )
    quantification_intermediate_output_filename = os.path.join(
        directory_for_the_output_of_this_script,
        sample_name.replace(".mzML", "") + "_quant_intermediate_TEST.tsv",
    )

    df_results = pd.DataFrame()
    df_results_min5_family = pd.DataFrame()
    df_results_min5 = pd.DataFrame()
    total_hit_count_contaminants = 0
    labeled_total_hit_count = 0
    taxonomic_columns = ["Kingdom", "Phylum", "Class", "Order", "Family", "Genus"]

    found_seqs = MetaProSIP_Output["Peptide Sequence"].tolist()
    found_seqs = [
        str(x).replace("(Carbamidomethyl)", "").replace("(Oxidation)", "")
        for x in found_seqs
    ]
    found_seqs_labeled = MetaProSIP_Output["Peptide Sequence"][
        MetaProSIP_Output["INT 2"] > 0
    ].tolist()  # if RIA 2 is reported, peptide is designated as labeled
    found_seqs_labeled = [
        str(x).replace("(Carbamidomethyl)", "").replace("(Oxidation)", "")
        for x in found_seqs_labeled
    ]

    # The found_seqs lists are extended by adding the isobaric equivalent if in the database
    # Peptides that are also in the contaminant database are excluded from found_seqs_wobbled
    found_seqs_wobbled = []
    found_contaminant_peps_wobbled = []
    found_seqs_labeled_wobbled = []

    for i in found_seqs_labeled:
        for s in make_patterns(i):
            if s in all_tryp_peps_set:
                if s not in all_contaminant_peps_set:
                    if s not in found_seqs_labeled_wobbled:
                        found_seqs_labeled_wobbled.append(s)

    for i in found_seqs:
        for s in make_patterns(i):
            if s in all_contaminant_peps_set:
                if s not in found_contaminant_peps_wobbled:
                    found_contaminant_peps_wobbled.append(s)
            if s in all_tryp_peps_set:
                if s not in all_contaminant_peps_set:
                    if s not in found_seqs_wobbled:
                        found_seqs_wobbled.append(s)

    # set the counts to 0 before start counting and reset the following dataframes
    df_database.loc[:, "HitPepSeq"] = str("")
    df_database.loc[:, "Count"] = 0
    df_database.loc[:, "labeled_Count"] = 0
    df_database.loc[:, "labeled_HitPepSeq"] = str("")
    df_HPSM = pd.DataFrame()
    df_results = pd.DataFrame()
    df_results_min5_family = pd.DataFrame()
    df_results_min5 = pd.DataFrame()
    total_hit_count = 0
    df_contaminants.loc[:, "HitPepSeq"] = str("")
    df_contaminants.loc[:, "Count"] = 0
    total_hit_count_contaminants = 0
    labeled_total_hit_count = 0
    taxonomic_columns = ["Kingdom", "Phylum", "Class", "Order", "Family", "Genus"]

    # loop through contaminant peptides
    for contaminant_peptide in found_contaminant_peps_wobbled:
        # List with True/False - true if peptide is in contaminant database
        mask = [contaminant_peptide in x for x in df_contaminants["TrypPeps"]]
        # for all proteins in the contaminant database containing this tryptic peptide:
        # "count" column is increased by 1 and the peptide is added to "HitPepSeq" column
        df_contaminants.loc[mask, "Count"] += 1
        df_contaminants.loc[mask, "HitPepSeq"] = (
            df_contaminants.loc[mask, "HitPepSeq"] + contaminant_peptide + str(",")
        )
        # print how often each peptide was found in the contaminant database
        print(
            contaminant_peptide,
            found_contaminant_peps_wobbled.index(contaminant_peptide) + 1,
            "/",
            len(found_contaminant_peps_wobbled),
            "No. of hits in the contaminant database:",
            len(df_contaminants[mask]),
        )
        total_hit_count_contaminants += len(df_contaminants[mask])
    # print how often contaminant peptides were found in total in the contaminant database
    print(
        "Total found hits over all contaminant peptides:", total_hit_count_contaminants
    )
    df_contaminants["HitPepSeq"] = df_contaminants.HitPepSeq.apply(
        lambda x: x.split(",")
    )
    # save contaminants dataframe as tsv-file
    df_contaminants.to_csv(python_contaminants_output_filename, sep="\t")

    # loop through the detected GroEL peptides
    for peptide in found_seqs_wobbled:
        # List with True/False - true if peptide is in GroEL database
        mask = [peptide in x for x in df_database["TrypPeps"]]
        # for all proteins in the GroEL database containing this tryptic peptide:
        # "count" column is increased by 1 and the peptide is added to "HitPepSeq" column
        df_database.loc[mask, "Count"] += 1
        df_database.loc[mask, "HitPepSeq"] = (
            df_database.loc[mask, "HitPepSeq"] + peptide + str(",")
        )
        # print how often each peptide was found in the GroEL database
        print(
            peptide,
            found_seqs_wobbled.index(peptide) + 1,
            "/",
            len(found_seqs_wobbled),
            "No. of hits in the database:",
            len(df_database[mask]),
        )
        total_hit_count += len(df_database[mask])
    print("Total found hits over all peptides:", total_hit_count)
    # List with True/False - true if peptide is in found_seqs_labeled_wobbled

    # loop through the detected labeled GroEL peptides
    for labeled_peptide in found_seqs_labeled_wobbled:
        labeled_mask = [labeled_peptide in x for x in df_database["TrypPeps"]]
        # for all proteins in the GroEL database containing this tryptic peptide:
        # "labeled count" column is increased by 1 and the peptide is added to "HitPepSeq" column
        df_database.loc[labeled_mask, "labeled_Count"] += 1
        df_database.loc[labeled_mask, "labeled_HitPepSeq"] = (
            df_database.loc[labeled_mask, "labeled_HitPepSeq"]
            + labeled_peptide
            + str(",")
        )
        print(
            labeled_peptide,
            found_seqs_labeled_wobbled.index(labeled_peptide) + 1,
            "/",
            len(found_seqs_labeled_wobbled),
            "No. of hits in the database:",
            len(df_database[labeled_mask]),
        )
        labeled_total_hit_count += len(df_database[labeled_mask])
    print("Total found hits over all labeled peptides:", labeled_total_hit_count)
    # print how often the peptides were found in total in the GroEL database

    # Create a new dataframe (df_HPSM) based on df_database that merges redundant sets of HitPepSeqs,
    # counts its peptides and gives respective Acc NOs (and count of Acc NOs) + taxonomy of matching proteins
    df_database = df_database.drop(
        columns=["ProtSeq", "TrypPeps", "Unnamed: 0"]
    )  # removes unnecessary columns
    df_database = df_database.astype(
        str
    )  # changes all columns to str, because this is necessary for the following functions
    df_database = df_database.groupby(df_database.HitPepSeq)
    df_HPSM = df_database.agg("first")
    df_HPSM.update(
        df_database.agg(
            {
                "AccNo": ",".join,
                "Kingdom": ",".join,
                "Phylum": ",".join,
                "Class": ",".join,
                "Order": ",".join,
                "Family": ",".join,
                "Genus": ",".join,
            }
        )
    )  # merges all rows with the same HitPepSeq into one row
    df_HPSM["AccNo_Count"] = df_HPSM["AccNo"].apply(
        lambda x: x.count(",") + 1
    )  # give the number of AccNos (number of GroEL proteins with that HitPepSeq)
    df_HPSM["Count"] = df_HPSM["Count"].astype(
        int
    )  # transforms 'Count' from str to int to sort in the next step
    df_HPSM = df_HPSM.sort_values(
        "Count", ascending=False
    )  # sorts the dataframe by 'Count' in descending order
    df_HPSM.reset_index(level=0, inplace=True)  # sets new index
    df_HPSM["HitPepSeq"] = df_HPSM["HitPepSeq"].str[
        :-1
    ]  # removes last ',' in HitPepSeq
    df_HPSM["labeled_HitPepSeq"] = df_HPSM["labeled_HitPepSeq"].str[:-1]
    df_HPSM.to_csv(python_intermediate_filename, sep="\t")  # saves df_HPMS as tsv-file

    # Create a new dataframe (df_results) based on df_HPSM
    df_results = df_HPSM.drop(df_HPSM.index[-1])  # removes last row (no peptide hits)
    df_results["HitPepSeq"] = df_results["HitPepSeq"].str.split(
        ","
    )  # transforms HitPepSeqs from str to lists
    df_results["labeled_HitPepSeq"] = df_results["labeled_HitPepSeq"].str.split(",")
    df_results[taxonomic_columns] = df_results[taxonomic_columns].apply(
        lambda col: col.str.split(",")
    )  # convert the taxonomic information of the respective GroEL account numbers to lists to faciliate analysis
    for (
        col
    ) in (
        taxonomic_columns
    ):  # create new columns with the most frequent taxonomic classification and its frequency in decimal at each level
        df_results["Top_" + col] = df_results[col].apply(lambda row: most_frequent(row))
        df_results["Top_" + col + "_fq"] = df_results[col].apply(
            lambda row: most_frequent_frequency_proportion(row)
        )

    # intermediate step to calculate TopRank_Count, shared_TopRank_Count, and strict_TopRank_Count:
    unique_count_list = df_results["Count"].unique().tolist()
    df_results["HitPepSeq_of_same_count"] = ""
    df_results["new_HitPepSeq"] = ""
    HitPepSeq_list = []
    # create a new column that contains all peptides detected for each Count ('HitPepSeq_of_same_count')
    # and a new column that contains all peptides detected for each Count but not for a higher Count ('new_HitPepSeq')
    for unique_count in unique_count_list:
        mask = df_results["Count"] == unique_count
        for each_count in df_results.loc[mask, "HitPepSeq"]:
            for each_peptide in each_count:
                df_results.loc[mask, "HitPepSeq_of_same_count"] = (
                    df_results.loc[mask, "HitPepSeq_of_same_count"] + each_peptide + ","
                )
                if each_peptide not in HitPepSeq_list:
                    HitPepSeq_list.append(each_peptide)
                    df_results.loc[mask, "new_HitPepSeq"] = (
                        df_results.loc[mask, "new_HitPepSeq"] + each_peptide + ","
                    )

    df_results["HitPepSeq_of_same_count"] = (
        df_results["HitPepSeq_of_same_count"]
        .str[:-1]
        .str.split(",")
        .apply(lambda row: remove_duplicates_from_list(row))
    )
    df_results["new_HitPepSeq"] = (
        df_results["new_HitPepSeq"]
        .str[:-1]
        .str.split(",")
        .apply(lambda row: remove_duplicates_from_list(row))
    )

    df_results["intersection_HitPepSeqs"] = [
        list(set(a).intersection(b))
        for a, b in zip(df_results.HitPepSeq, df_results.new_HitPepSeq)
    ]
    if total_hit_count > 0:
        df_results["len_intersection_HitPepSeqs"] = df_results[
            "intersection_HitPepSeqs"
        ].str.len()

    list_of_all_intersection_HitPepSeqs = list(
        itertools.chain.from_iterable(df_results["intersection_HitPepSeqs"].tolist())
    )
    shared_Top_HitPepSeqs = get_duplicates_from_list(
        list_of_all_intersection_HitPepSeqs
    )

    df_results["shared_Top_HitPepSeqs"] = df_results["intersection_HitPepSeqs"].apply(
        lambda row: get_intersection_from_two_lists(row, shared_Top_HitPepSeqs)
    )
    df_results["single_Top_HitPepSeqs"] = (
        df_results["intersection_HitPepSeqs"].map(set)
        - df_results["shared_Top_HitPepSeqs"].map(set)
    ).map(list)

    # adds new columns:  TopRank_Count, shared_TopRank_Count, and strict_TopRank_Count.
    # strict_TopRank_Count: number of peptides not present in another HitPepSeq with same or higher peptide count
    # shared_TopRank_Count: number of peptides present in another HitPepSeq with the exact same peptide count.
    # The sum of strict and shared TopRank_Count is called TopRank_Count. On default, we use this value for filtering.
    if total_hit_count > 0:
        df_results["shared_TopRank_Count"] = df_results[
            "shared_Top_HitPepSeqs"
        ].str.len()
        df_results["strict_TopRank_Count"] = (
            df_results["len_intersection_HitPepSeqs"]
            - df_results["shared_TopRank_Count"]
        )
        df_results["TopRank_Count"] = df_results["len_intersection_HitPepSeqs"]

    # if no peptides were found, all Counts are = 0
    if total_hit_count == 0:
        df_results["shared_TopRank_Count"] = 0
        df_results["strict_TopRank_Count"] = 0
        df_results["TopRank_Count"] = 0

    # rearrange columns to improve readability
    df_results = df_results[
        [
            "HitPepSeq",
            "Count",
            "labeled_HitPepSeq",
            "labeled_Count",
            "TopRank_Count",
            "shared_Top_HitPepSeqs",
            "shared_TopRank_Count",
            "single_Top_HitPepSeqs",
            "strict_TopRank_Count",
            "AccNo",
            "AccNo_Count",
            "Kingdom",
            "Top_Kingdom",
            "Top_Kingdom_fq",
            "Phylum",
            "Top_Phylum",
            "Top_Phylum_fq",
            "Class",
            "Top_Class",
            "Top_Class_fq",
            "Order",
            "Top_Order",
            "Top_Order_fq",
            "Family",
            "Top_Family",
            "Top_Family_fq",
            "Genus",
            "Top_Genus",
            "Top_Genus_fq",
        ]
    ]

    # save df_results to tsv-file
    df_results.to_csv(python_output_filename, sep="\t")

    # Filter df_results to create new dataframe based on taxonomy and quantify
    print("Creating family-based output file...")
    family_based_analysis_output_filename = os.path.join(
        directory_for_the_output_of_this_script,
        sample_name.replace(".mzML", "") + "_family_based_min5.tsv",
    )
    df_results_min5 = df_results[
        df_results["TopRank_Count"] > 4
    ]  # by default GroEL protein groups are filtered by a TopRank_Count >4.
    # If you want to adjust this, change here.

    df_results_min5_family = df_results_min5.groupby("Top_Family").agg(
        {"HitPepSeq": "sum", "labeled_HitPepSeq": "sum"}
    )  # by default GroEl protein groups are merged at the family level.
    # If you want to adjust this, change here to the level of interest.
    df_results_min5_family.reset_index(level=0, inplace=True)

    df_results_min5_family["HitPepSeq"] = df_results_min5_family["HitPepSeq"].apply(
        lambda row: remove_duplicates_from_list(row)
    )
    df_results_min5_family["labeled_HitPepSeq"] = df_results_min5_family[
        "labeled_HitPepSeq"
    ].apply(lambda row: remove_duplicates_from_list(row))

    df_results_min5_family["non_redundant_peptide_count"] = 0
    df_results_min5_family["non_redundant_peptide_count"] = df_results_min5_family[
        "HitPepSeq"
    ].apply(lambda row: len(row))
    df_results_min5_family["non_redundant_labeled_peptide_count"] = 0
    df_results_min5_family[
        "non_redundant_labeled_peptide_count"
    ] = df_results_min5_family["labeled_HitPepSeq"].apply(lambda row: len(row))

    df_results_min5_family["Int"] = 0
    df_results_min5_family["weighted Int"] = 0

    df_test = df_results_min5_family.drop(columns=["labeled_HitPepSeq"])

    df_test = df_test.explode(
        "HitPepSeq"
    )  # Explode the HitPepSeq column so that each peptide sequence has its own row in the dataframe.
    df_test["IL_possibilites"] = df_test["HitPepSeq"].apply(
        lambda x: list(make_patterns(x))
    )  # Generates all possible IL patterns for each peptide sequence.
    df_test.reset_index(level=0, inplace=True)

    # Calculate the intensity for each peptide sequence derived from the MetaProSIP_Output.
    df_test["Int"] = df_test["IL_possibilites"].apply(
        lambda x: (
            sum(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                ]["INT 1"]
            )
        )
    )
    df_test["labeled_Int1"] = df_test["IL_possibilites"].apply(
        lambda x: (
            sum(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["INT 2"].notnull()
                ]["INT 2"]
            )
        )
    )
    df_test["labeled_Corr1"] = df_test["IL_possibilites"].apply(
        lambda x: (
            statistics.mean(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["Cor. 2"].notnull()
                ]["Cor. 2"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["Cor. 2"].notnull()
            ]["Cor. 2"].any()
            else None
        )
    )
    df_test["RIA 1"] = df_test["IL_possibilites"].apply(
        lambda x: (
            median_ignore_empty_and_zero(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["RIA 2"].notnull()
                ]["RIA 2"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["RIA 2"].notnull()
            ]["RIA 2"].any()
            else None
        )
    )
    df_test["labeled_Int2"] = df_test["IL_possibilites"].apply(
        lambda x: (
            sum(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["INT 3"].notnull()
                ]["INT 3"]
            )
        )
    )
    df_test["labeled_Corr2"] = df_test["IL_possibilites"].apply(
        lambda x: (
            statistics.mean(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["Cor. 3"].notnull()
                ]["Cor. 3"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["Cor. 3"].notnull()
            ]["Cor. 3"].any()
            else None
        )
    )
    df_test["RIA 2"] = df_test["IL_possibilites"].apply(
        lambda x: (
            median_ignore_empty_and_zero(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["RIA 3"].notnull()
                ]["RIA 3"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["RIA 3"].notnull()
            ]["RIA 3"].any()
            else None
        )
    )
    df_test["labeled_Int3"] = df_test["IL_possibilites"].apply(
        lambda x: (
            sum(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["INT 4"].notnull()
                ]["INT 4"]
            )
        )
    )
    df_test["labeled_Corr3"] = df_test["IL_possibilites"].apply(
        lambda x: (
            statistics.mean(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["Cor. 4"].notnull()
                ]["Cor. 4"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["Cor. 4"].notnull()
            ]["Cor. 4"].any()
            else None
        )
    )
    df_test["RIA 3"] = df_test["IL_possibilites"].apply(
        lambda x: (
            median_ignore_empty_and_zero(
                MetaProSIP_Output[
                    MetaProSIP_Output["Peptide Sequence"]
                    .apply(
                        lambda y: y.replace("(Carbamidomethyl)", "").replace(
                            "(Oxidation)", ""
                        )
                        if isinstance(y, str)
                        else y
                    )
                    .isin(x)
                    & MetaProSIP_Output["RIA 4"].notnull()
                ]["RIA 4"]
            )
            if MetaProSIP_Output[
                MetaProSIP_Output["Peptide Sequence"]
                .apply(
                    lambda y: y.replace("(Carbamidomethyl)", "").replace(
                        "(Oxidation)", ""
                    )
                    if isinstance(y, str)
                    else y
                )
                .isin(x)
                & MetaProSIP_Output["RIA 4"].notnull()
            ]["RIA 4"].any()
            else None
        )
    )

    # create intermediate df to count how often peptides were identified in different taxonomic groups
    df_intermediate = df_test.filter(
        [
            "IL_possibilites",
            "non_redundant_peptide_count",
            "non_redundant_labeled_peptide_count",
        ],
        axis=1,
    )
    df_intermediate["IL_possibilites"] = df_intermediate["IL_possibilites"].astype(str)
    df_intermediate = df_intermediate.groupby(df_intermediate.IL_possibilites).agg(
        "sum"
    )
    df_intermediate.reset_index(level=0, inplace=True)
    df_intermediate["intermediate"] = df_intermediate["IL_possibilites"]
    df_test["intermediate"] = df_test["IL_possibilites"].astype(str)
    merged_df = df_test.merge(
        df_test.merge(df_intermediate, how="left", on="intermediate", sort=False)
    )

    # Calculate weighted intensity by attributing the precursor intensity of shared peptides
    # proportionally to each taxonomic group based on the total number of peptides assigned to that group
    df_test[
        "total number of peptides of taxonomic groups that this peptide was assigned to"
    ] = merged_df["non_redundant_peptide_count_y"]
    df_test[
        "total number of labeled peptides of taxonomic groups that this peptide was assigned to"
    ] = merged_df["non_redundant_labeled_peptide_count_y"]
    df_test["weighted Int"] = (
        df_test["Int"]
        * df_test["non_redundant_peptide_count"]
        / df_test[
            "total number of peptides of taxonomic groups that this peptide was assigned to"
        ]
    )
    df_test["labeled_weighted Int"] = (
        df_test["labeled_Int1"]
        * df_test["non_redundant_labeled_peptide_count"]
        / df_test[
            "total number of labeled peptides of taxonomic groups that this peptide was assigned to"
        ]
    )
    df_test["labeled_weighted Int2"] = (
        df_test["labeled_Int2"]
        * df_test["non_redundant_labeled_peptide_count"]
        / df_test[
            "total number of labeled peptides of taxonomic groups that this peptide was assigned to"
        ]
    )
    df_test["labeled_weighted Int3"] = (
        df_test["labeled_Int3"]
        * df_test["non_redundant_labeled_peptide_count"]
        / df_test[
            "total number of labeled peptides of taxonomic groups that this peptide was assigned to"
        ]
    )

    # Calculate LR of RIA's
    df_test["LR(RIA 1)"] = df_test["labeled_Int1"] / (
        df_test["Int"] + df_test["labeled_Int1"]
    )
    df_test["LR(RIA 2)"] = df_test["labeled_Int2"] / (
        df_test["Int"] + df_test["labeled_Int2"]
    )
    df_test["LR(RIA 3)"] = df_test["labeled_Int3"] / (
        df_test["Int"] + df_test["labeled_Int3"]
    )

    df_test["#TGs in which peptide appears"] = df_test.intermediate.map(
        df_test.intermediate.value_counts()
    )
    df_test.drop("intermediate", axis=1, inplace=True)
    df_test.to_csv(quantification_intermediate_output_filename, sep="\t")

    # merge output to increase readability.
    df_test = df_test.drop(
        columns=[
            "non_redundant_peptide_count",
            "total number of peptides of taxonomic groups that this peptide was assigned to",
        ]
    )
    df_test2 = df_test.groupby("Top_Family").agg(
        {
            "HitPepSeq": list,
            "Int": "sum",
            "weighted Int": "sum",
            "labeled_Int1": "sum",
            "labeled_weighted Int": "sum",
            "labeled_Int2": "sum",
            "labeled_weighted Int2": "sum",
            "labeled_Int3": "sum",
            "labeled_weighted Int3": "sum",
            "RIA 1": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
            "LR(RIA 1)": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
            "RIA 2": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
            "LR(RIA 2)": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
            "RIA 3": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
            "LR(RIA 3)": lambda x: median_ignore_empty_and_zero(
                x[df_test["#TGs in which peptide appears"] == 1]
            ),
        }
    )
    df_test2.reset_index(level=0, inplace=True)
    df_test2["HitPepSeq"] = df_test2["HitPepSeq"].apply(
        lambda x: remove_duplicates_from_list(x)
    )
    df_test2["nr_Peptide_count"] = df_test2["HitPepSeq"].apply(lambda x: len(x))
    all_peptides = [
        peptide for peptides_list in df_test2["HitPepSeq"] for peptide in peptides_list
    ]
    df_test2["Unique_HitPepSeq"] = df_test2["HitPepSeq"].apply(
        lambda row: [peptide for peptide in row if all_peptides.count(peptide) == 1]
    )
    df_test2["Unique_HitPepCount"] = df_test2["Unique_HitPepSeq"].apply(
        lambda x: len(x)
    )
    df_test2["labeled_HitPepSeq"] = df_test2["HitPepSeq"].apply(
        lambda x: set(x).intersection(found_seqs_labeled_wobbled)
    )
    df_test2["nr_labeled_Peptide_count"] = df_test2["HitPepSeq"].apply(
        lambda x: len(set(x).intersection(found_seqs_labeled_wobbled))
    )
    all_labeled_peptides = [
        labeled_peptide
        for labeled_peptides_list in df_test2["labeled_HitPepSeq"]
        for labeled_peptide in labeled_peptides_list
    ]
    df_test2["Unique_labeled_HitPepSeq"] = df_test2["labeled_HitPepSeq"].apply(
        lambda row: [
            labeled_peptide
            for labeled_peptide in row
            if all_labeled_peptides.count(labeled_peptide) == 1
        ]
    )
    df_test2["Unique_labeled_HitPepCount"] = df_test2["Unique_labeled_HitPepSeq"].apply(
        lambda x: len(x)
    )
    df_test2.reset_index(level=0, inplace=True)
    df_test2 = df_test2.drop(columns=["index"])

    df_test2.to_csv(family_based_analysis_output_filename, sep="\t")
    print("family-based output file created sucessfully")
